<ul class="errors">
	<c:forEach items="${errors}" var="error">
		<li>
			${error.category}: <!-- o campo em que ocorreu o erro, ou o tipo do erro -->
			${error.message} <!-- a mensagem de erro de validação -->
		</li>
	</c:forEach>
</ul>
<form action="${linkTo[LivrosController].salva}" method="post" enctype="multipart/form-data">
	<input type="hidden" name="livro.id" value="${livro.id}" />
	<input type="hidden" name="_method" value="PUT" />
	<h2>Formulário de cadastro de livros</h2>
	<ul>
		<li>Título: <br/>
		 <input type="text" name="livro.titulo" value="${livro.titulo}"/>  </li>
		 
		<li>Descrição: <br/>
		 <textarea type="text" name="livro.descricao">${livro.descricao}</textarea>  </li>
		 
		<li>ISBN: <br/>
		 <input type="text" name="livro.isbn" value="${livro.isbn}"/>  </li>
		 
		<li>Moeda: <br/>
		 <input type="text" name="livro.preco.moeda" value="${livro.preco.moeda}"/>  </li>
		 
		 <li>Montante: <br/>
		 <input type="text" name="livro.preco.montante" value="${livro.preco.montante}"/>  </li>
		 
		<li>Data de publicação: <br/>
		 <input type="text" name="livro.dataPublicacao" value="${livro.dataPublicacao}" />  </li>
		 
		 <li>Capa: <br/>
		 <input type="file" name="capa" />  </li>
	</ul>
	<input type="submit" value="Salvar" />
</form>