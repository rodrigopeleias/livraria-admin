<c:if test="${not empty mensagem}">
	<p class="mensagem">
		${mensagem}
	</p>
</c:if>
<h3>Lista de Livros</h3>
<ul>
<c:forEach items="${livros} var="livro">
	<li>${livro.titulo} - ${livro.descricao}</li>
	<img src="${linkTo[LivrosController].capa(livro.isbn)}" 
	width="70" height="100"/>
	<a href="${linkTo[LivrosController].edita(livro.isbn)}" >
		Modificar
	</a>
</c:forEach>
</ul>