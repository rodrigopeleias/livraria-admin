package br.com.casadocodigo.livraria.controlador;

import java.io.IOException;
import java.net.URI;
import java.util.Calendar;

import javax.inject.Inject;
import javax.validation.Valid;

import com.google.common.io.ByteStreams;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.observer.download.Download;
import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.caelum.vraptor.validator.Validator;
import br.com.casadocodigo.livraria.interceptadores.Transacional;
import br.com.casadocodigo.livraria.modelo.Arquivo;
import br.com.casadocodigo.livraria.modelo.ArquivoDownload;
import br.com.casadocodigo.livraria.modelo.Diretorio;
import br.com.casadocodigo.livraria.modelo.Estante;
import br.com.casadocodigo.livraria.modelo.Livro;

@Controller
public class LivrosController {
	
	private Estante estante;
	private Result result;
	private Validator validator;
	private Diretorio imagens;
	
	@Inject
	public LivrosController(Estante estante, Result result, Validator validator, Diretorio imagens) {
		this.estante = estante;
		this.result = result;
		this.validator = validator;
		this.imagens = imagens;
	}
	
	/**
	 * @deprecated REquerido pelo CDI, para injeção por construtor
	 */
	LivrosController() {		
	}
	
	public void formulario() {
	}

	@Transacional
	@Post("/livros")
	public void salva(@Valid Livro livro, UploadedFile capa, Result result) throws IOException{
		validator.onErrorRedirectTo(this).formulario();
		
		if (capa != null) {
			URI imagemCapa = imagens.grava(
					new Arquivo(capa.getFileName(), ByteStreams.toByteArray(capa.getFile()), 
							capa.getContentType(), Calendar.getInstance()));
			livro.setCapa(imagemCapa);
		}
		estante.guarda(livro);		
		result.include("mensagem", "Livro salvo com sucesso!");
		result.redirectTo(this).lista();		
	}
	
	@Get("/livros")
	public void lista() {	
		this.result.include("promocao", this.estante.promocaoDoDia());
		this.result.include("livros", this.estante.todosOsLivros());
	}
	
	@Get @Path(value="/livros/{isbn}", priority=Path.LOWEST)
	public void edita(String isbn, Result result) {		
		Livro livroEncontrado = estante.buscaPorIsbn(isbn);
		if (livroEncontrado == null) {
			result.notFound();	
		} else {
			//queremos o resultado deste controller no método formulário
			result.include(livroEncontrado);
			result.of(this).formulario();
		}		
	}	
	
	@Put("/livros/{livro.isbn}")
	public void altera(Livro livro) {
		//salva as alterações de um livro
	}
	
	@Delete("/livros/{livro.isbn}")
	public void remove(Livro livro) {
		//remove um livro
	}
	
	@Get("/livros/{isbn}/capa")
	public Download capa(String isbn) {
		Livro livro = estante.buscaPorIsbn(isbn);		
		Arquivo capa = imagens.recupera(livro.getCapa());
		if (capa == null) {
			result.notFound();
			return null;
		}
		return new ArquivoDownload(capa);		
	}
}
