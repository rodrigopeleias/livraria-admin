package br.com.casadocodigo.livraria.interceptadores;

import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.AfterCall;
import br.com.caelum.vraptor.BeforeCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.casadocodigo.livraria.autenticacao.Usuario;

@Intercepts
public class AuditoriaInterceptor {
	
	private @Inject Logger logger;
	private @Inject Usuario usuario;
	private @Inject HttpServletRequest request;
	
	@BeforeCall
	public void loga() {
		logger.info("O usuario " + usuario.getNome() + "  acessou {}" + request.getRequestURI());
	}
	
	@AfterCall
	public void validaLog() {
		logger.info("O usuario " + usuario.getNome() + "  acabou de sair da página {}" + request.getRequestURI());
	}

}
