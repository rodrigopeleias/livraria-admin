package br.com.casadocodigo.livraria.interceptadores;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.AcceptsWithAnnotations;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;

@Intercepts
@AcceptsWithAnnotations(Transacional.class)
public class TransacoesInterceptor{

	private EntityManager manager;
	
	@Inject
	public TransacoesInterceptor(EntityManager manager) {
		this.manager = manager;
	}
	
	@Deprecated TransacoesInterceptor() {
	}
	
	@AroundCall
	public void trataTransacao(SimpleInterceptorStack stack) {
		try {
			manager.getTransaction().begin();
			stack.next();
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive()) {
				manager.getTransaction().rollback();
			}
		}		
	}
	
	// uma das formas de adicionar suporte às transações em métodos anotados
	/*@Accepts
	public boolean ehTransacional(ControllerMethod method) {
		return method.containsAnnotation(Transacional.class);
	}*/
}
