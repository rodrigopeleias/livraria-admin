package br.com.casadocodigo.livraria.repositorio;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

//Dependent indica que o ciclo de vida da dependnÊncia irá durar o mesmo tempo que o objeto
//a receber esta dependnência sobreviver
@Dependent
public class FabricaDeJPA {
	
	//Configuração JPA para Servidores de aplicação como Glassfish ou WildFly
	
	@PersistenceContext
	private EntityManager manager;
	
	@PersistenceUnit
	private EntityManagerFactory factory;
	
	@Produces
	public EntityManager getManager() {
		return manager;
	}
	
	//Criação de EntityManager para containers web como Tomcat e Jetty
	//applicationScoped cria apenas uma instância em todo o ciclo de vida da aplicação - "singleton
	/*@Produces
	@ApplicationScoped 
	public EntityManagerFactory criaEntityManagerFactory() {
		return Persistence.createEntityManagerFactory("default");
	}
	
	@Produces
	public EntityManager criaEntityManager(EntityManagerFactory factory) {		
		return factory.createEntityManager();
	}
	
	//Disposes indica o momento de inutilização de um objeto
	public void fechaManager(@Disposes EntityManager manager) {
		manager.close();
	}
	
	public void fechaFactory(@Disposes EntityManagerFactory factory) {
		factory.close();
	}*/

}
