package br.com.casadocodigo.livraria.autenticacao;

public interface RegistroDeUsuarios {
	
	Usuario comLoginESenha(String login, String senha);

}
