package br.com.casadocodigo.livraria.autenticacao;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

@SessionScoped
public class UsuarioLogado implements Serializable{
	
	private static final long serialVersionUID = -907353709674910676L;
	
	private Usuario usuario;
	
	public void loga(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public boolean isLogado() {
		return this.usuario != null;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void desloga() {
		this.usuario = null;
	}
}
