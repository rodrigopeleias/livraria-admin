package br.com.casadocodigo.livraria.autenticacao;

public class Usuario {
	
	private String login;
	private String senha;
	private String nome;
	
	private boolean admin;

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public String getLogin() {
		return login;
	}

	public String getSenha() {
		return senha;
	}

	public String getNome() {
		return nome;
	}				
}
